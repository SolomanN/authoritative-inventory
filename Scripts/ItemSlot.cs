namespace NetworkAuthority.Inventory
{
    public class ItemSlot
    {
        public ItemSlot(ItemCollection collection, int index)
        {
            this.collection = collection;
            this.index = index;
        }
        
        public int index { get; }
        public ItemCollection collection { get; }
        public ItemReference currentItem { get; private set; }

        public event ItemReference.ItemStackChangedDelegate ItemStackChanged;

        /// <summary>
        /// Set the item reference in this slot.
        /// </summary>
        /// <param name="newItem"></param>
        public void SetItem(ItemReference newItem)
        {
            if (currentItem != null)
                currentItem.StackChanged -= OnStackChanged;

            if (newItem != null)
                newItem.StackChanged += OnStackChanged;

            currentItem = newItem;
        }

        private void OnStackChanged(ItemReference item, int stack)
        {
            ItemStackChanged?.Invoke(item, stack);
        }

        /// <summary>
        /// Use this item slot.
        /// </summary>
        public void Use()
        {
            if (currentItem == null)
                return;
            
            collection.inventory.Use(currentItem);
        }
    }
}