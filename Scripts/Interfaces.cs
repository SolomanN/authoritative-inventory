using UnityEngine.EventSystems;

namespace NetworkAuthority.Inventory
{
    public interface IDragable : IBeginDragHandler, IDragHandler, IEndDragHandler
    { }

    public interface IPointerDetectable : IPointerEnterHandler, IPointerExitHandler
    { }

    public interface IItemSlotRenderer : IRepaintable, IPointerClickHandler, IDragable
    { }

    public interface IItemReferencable
    {
        ItemReference GetCurrentItem();
    }

    public interface IStackable
    {
        int itemStack { get; }
        int maxItemStack { get; }
    }
    
    public interface IInventoryAuthorizable
    {
        void Initialize(Inventory inventory);
        bool AuthorizeItemAdd(ItemInstanceBase item);
        bool AuthorizeItemDrop(ItemCollection collection, ItemReference item);
        bool AuthorizeItemDestroy(ItemInstanceBase item);
        bool AuthorizeItemUse(ItemCollection collection, ItemReference itemReference);
        bool AuthorizeItemMove(ItemReference reference, ItemCollection collection, int slot);
    }

    public interface IInventoryCallbacks
    {
        void OnStackChanged(ItemCollection collection, ItemReference reference);
        void OnSlotChanged(ItemCollection collection, ItemReference reference, int slot);
        void OnSlotCleared(ItemCollection collection, int slot);
        void OnItemInstanceCreated(ItemInstanceBase instance);
    }
    
    public interface IRepaintable
    {
        void Repaint();
    }

    public interface IItemUser
    {
        void Use(ItemCollection collection, ItemReference reference);
    }
}