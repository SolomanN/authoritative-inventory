using System;

namespace NetworkAuthority.Inventory
{
    public class ItemReference : IStackable
    {
        public delegate void ItemStackChangedDelegate(ItemReference item, int stack);
        public delegate void ItemDropped(ItemReference reference);

        private int _itemStack;
        
        public ItemReference(ItemBase asset, int itemStack, int maxItemStack, Guid id)
        {
            this.asset = asset;
            _itemStack = itemStack;
            this.maxItemStack = maxItemStack;
            this.id = id;
        }
        
        /// <summary>
        /// The id of this reference.
        /// </summary>
        public Guid id { get; private set; }

        /// <summary>
        /// The item asset tied to this reference.
        /// </summary>
        public ItemBase asset { get; }

        /// <summary>
        /// The item reference stack.
        /// </summary>
        public int itemStack
        {
            get => _itemStack;
            set
            {
                _itemStack = value;
                if (value != _itemStack)
                {
                    StackChanged?.Invoke(this, _itemStack);
                }
            } 
        }
        
        /// <summary>
        /// The max item reference stack.
        /// </summary>
        public int maxItemStack { get; }

        public event ItemStackChangedDelegate StackChanged;
        public event ItemDropped Dropped;

        /// <summary>
        /// Creates an item from the given instance.
        /// </summary>
        /// <param name="instance">The item instance.</param>
        /// <returns></returns>
        public static ItemReference FromInstance(ItemInstanceBase instance)
        {
            return new ItemReference(instance.asset, instance.itemStack, instance.maxItemStack, Guid.NewGuid());
        }

        /// <summary>
        /// True if the item stack is full.
        /// </summary>
        /// <returns></returns>
        public bool IsFull()
        {
            return itemStack >= maxItemStack;
        }

        /// <summary>
        /// True if the item stack is empty. 
        /// </summary>
        /// <returns></returns>
        public bool IsEmpty()
        {
            return itemStack <= 0;
        }

        /// <summary>
        /// True if this is the same item type as the given reference.
        /// </summary>
        /// <param name="reference"></param>
        /// <returns></returns>
        public bool IsTheSameAs(ItemReference reference)
        {
            return reference.asset.itemId == asset.itemId;
        }

        public void OnDropped()
        {
            Dropped?.Invoke(this);
        }
    }
}