using UnityEngine;

namespace NetworkAuthority.Inventory.UI.Utils
{
    internal static class UIUtils
    {
        public static Vector2 KeepWithinScreenBounds(Canvas canvas, RectTransform transform, Vector2 position)
        {
            Vector2 retVal = position;
            Vector2 size = canvas.transform.TransformVector(transform.sizeDelta);
            
            if (retVal.x + size.x > Screen.width) retVal.x -= size.x;
            if (retVal.y + size.y > Screen.height) retVal.y -= size.y;          
            
            return retVal;
        }
    }
}