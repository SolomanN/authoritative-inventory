using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;

namespace NetworkAuthority.Inventory
{
    public partial class ItemDatabase : MonoBehaviour
    {
        [HideInInspector]
        [SerializeField] private List<ItemDescriptor> itemAssets = null;
        private static ItemDatabase _instance;

        /// <summary>
        /// The item asset count in the active database.
        /// </summary>
        public static int Count => _instance != null ? _instance.itemAssets.Count : 0;

        private void Awake()
        {
            _instance = this;
            itemAssets = itemAssets.Where(x => x != null).ToList();
        }
        
        /// <summary>
        /// Gets an item descriptor with the given GUID.
        /// </summary>
        /// <param name="id">The item ID.</param>
        /// <returns></returns>
        public static ItemDescriptor GetAsset(Guid id)
        {
            return _instance == null ? null : _instance.itemAssets.Find(x => x.itemId == id.ToString());
        }

#if UNITY_EDITOR

#endif
    }
}