using UnityEngine;

namespace NetworkAuthority.Inventory.UI
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Inventory))]
    public class InventoryRenderer : MonoBehaviour
    {
        [SerializeField] private GameObject rendererPrefab = null;

        private Inventory inventory;

        private void Awake()
        {
            inventory = GetComponent<Inventory>();
        }

        private void Start()
        {
            Initialize();
        }

        public virtual void Initialize()
        {
            var renInst = Instantiate(rendererPrefab);
            var renderers = renInst.GetComponentsInChildren<ItemCollectionRenderer>();
            foreach (var ren in renderers)
            {
                var col = inventory.GetCollection(ren.collectionName);
                if (col == null) continue;
                ren.Initialize(col);
                ren.Repaint();
                
                Debug.Log($"Inventory: Spawned collection renderer for '{col.collectionName}'.");
            }
        }
    }
}