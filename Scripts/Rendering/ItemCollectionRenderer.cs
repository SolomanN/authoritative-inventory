﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace NetworkAuthority.Inventory.UI
{
    [DisallowMultipleComponent]
    public class ItemCollectionRenderer : UIBehaviour, IRepaintable
    {
        [SerializeField] private string _collectionName = null;
        [SerializeField] private GameObject slotPrefab = null;
        [SerializeField] private RectTransform container = null;

        public ItemCollection collection { get; private set; }
        public ItemSlotRenderer[] itemSlotRenderers { get; private set; }
        public string collectionName => _collectionName;

        public void Initialize(ItemCollection collection)
        {
            this.collection = collection;
            this.collection.SlotChanged += OnSlotChanged;
            this.collection.SlotCleared += OnSlotCleared;
            
            InitializeSlotRenderers();
        }

        private void OnSlotCleared(ItemCollection collection, int slot)
        {
            itemSlotRenderers[slot].Repaint();
        }

        private void OnSlotChanged(ItemCollection collection, ItemReference item, int slot)
        {
            itemSlotRenderers[slot].Repaint();
        }

        protected virtual void InitializeSlotRenderers()
        {
            itemSlotRenderers = new ItemSlotRenderer[collection.Count];
            for (int i = 0; i < collection.Count; i++)
            {
                var slot = Instantiate(slotPrefab, container);
                var slotRenderer = slot.GetComponent<ItemSlotRenderer>();
                itemSlotRenderers[i] = slotRenderer;
                slotRenderer.Initialize(collection.GetSlot(i));
            }
        }

        public void Repaint()
        {
            if (collection == null)
                return;

            for (var i = 0; i < collection.Count; i++)
            {
                itemSlotRenderers[i].Repaint();
            }
        }
    }
}
