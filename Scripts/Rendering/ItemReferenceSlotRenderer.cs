using UnityEngine.EventSystems;

namespace NetworkAuthority.Inventory.UI
{
    public class ItemReferenceSlotRenderer : ItemSlotRendererBase
    {
        public Inventory _inventory { get; set; }
        public ItemReference _currentItem { get; set; }

        protected override void Awake()
        {
            base.Awake();
            Repaint();
        }

        protected override void ExecuteUse()
        {
            _inventory.Use(_currentItem);
        }

        protected override void ExecuteDrop()
        {
            Clear();
        }

        private void Clear()
        {
            _currentItem = null;
            _inventory = null;
            Repaint();
        }

        protected override void ExecuteMove(ItemSlotRendererBase hoverSlot)
        {
            if (hoverSlot is ItemReferenceSlotRenderer rfr)
            {
                ItemReference temp = rfr._currentItem;
                Inventory tempInv = rfr._inventory;
                rfr.SetItem(_currentItem, _inventory);
                SetItem(temp, tempInv);
            }
        }

        public override ItemReference GetCurrentItem()
        {
            return _currentItem;
        }

        public virtual void SetItem(ItemReference newItem, Inventory slotOwner)
        {
            _inventory = slotOwner;
            UnRegisterEvents();
            _currentItem = newItem;
            RegisterEvents();
            Repaint();
        }

        private void RegisterEvents()
        {
            if (_currentItem == null) return;
            _currentItem.Dropped += OnItemDropped;
            _currentItem.StackChanged += OnItemStackChanged;
        }

        private void UnRegisterEvents()
        {
            if (_currentItem == null) return;
            _currentItem.Dropped -= OnItemDropped;
            _currentItem.StackChanged -= OnItemStackChanged;
        }

        private void OnItemStackChanged(ItemReference itemReference, int stack)
        {
            Repaint();
        }

        private void OnItemDropped(ItemReference item)
        {
            Clear();
        }
    }
}