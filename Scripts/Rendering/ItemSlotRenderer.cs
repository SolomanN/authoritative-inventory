using System.Collections.Generic;

namespace NetworkAuthority.Inventory.UI
{
    public class ItemSlotRenderer : ItemSlotRendererBase
    {
        /// <summary>
        /// The item slot this renderer is tied to.
        /// </summary>
        public ItemSlot slot { get; private set; }

        public void Initialize(ItemSlot itemSlot)
        {
            slot = itemSlot;
            slot.ItemStackChanged += SlotOnItemStackChanged;
        }

        public override ItemReference GetCurrentItem()
        {
            return slot.currentItem;
        }

        protected override void ExecuteUse()
        {
            slot.Use();
        }

        protected override void ExecuteDrop()
        {
            slot.collection.inventory.Drop(slot.currentItem);
        }

        protected override void ExecuteMove(ItemSlotRendererBase hoverSlot)
        {
            switch (hoverSlot)
            {
                case ItemSlotRenderer ren:
                    slot.collection.inventory.Move(slot.currentItem, ren.slot.collection, ren.slot.index);
                    break;
                case ItemReferenceSlotRenderer rfr:
                    rfr.SetItem(slot.currentItem, slot.collection.inventory);
                    break;
            }
        }

        private void SlotOnItemStackChanged(ItemReference item, int stack)
        {
            Repaint();
        }
    }
}