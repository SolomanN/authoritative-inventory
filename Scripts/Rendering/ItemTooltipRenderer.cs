using UnityEngine.EventSystems;

namespace NetworkAuthority.Inventory.UI
{
    public class ItemTooltipRenderer : UIBehaviour, IPointerDetectable
    {
        private bool drawTooltip;
        private IItemReferencable referencable;
        
        public ItemReference currentItem => referencable?.GetCurrentItem();

        protected override void Awake()
        {
            base.Awake();
            referencable = GetComponent<IItemReferencable>();
        }

        protected virtual void Update()
        {
            if (!drawTooltip) return;
            DrawTooltip(currentItem);
        }

        protected virtual void DrawTooltip(ItemReference reference)
        {
            if (reference == null) return;
            TooltipRenderer.DrawTooltip(reference);
        }

        public virtual void OnPointerEnter(PointerEventData eventData)
        {
            if (currentItem == null) return;
            drawTooltip = true;
        }

        public virtual void OnPointerExit(PointerEventData eventData)
        {
            drawTooltip = false;
        }
    }
}