using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace NetworkAuthority.Inventory.UI
{
    public abstract class ItemSlotRendererBase : UIBehaviour, IItemSlotRenderer, IItemReferencable
    {
        [SerializeField] private Image iconImage = null;
        [SerializeField] private TMP_Text stackText = null;

        private GameObject dragPreview;

        public virtual void Repaint()
        {
            if (GetCurrentItem() == null)
            {
                iconImage.enabled = false;
                if (stackText != null)
                {
                    stackText.text = string.Empty;
                }
            }
            else
            {
                var asset = GetCurrentItem().asset;
                iconImage.enabled = true;
                iconImage.sprite = asset.icon;

                if (stackText != null)
                    stackText.text = GetCurrentItem().maxItemStack > 1 ? $"x{GetCurrentItem().itemStack}" : string.Empty;
            }

            if (dragPreview != null)
            {
                Destroy(dragPreview);
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (GetCurrentItem() == null)
                return;
            
            if (eventData.clickCount == 2 && eventData.button == 0)
            {
                ExecuteUse();
            }
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (GetCurrentItem() == null)
                return;
            
            // Instantiate drag preview
            dragPreview = new GameObject($"Drag Preview: {GetCurrentItem().asset.itemName}");
            dragPreview.transform.SetParent(GetComponentInParent<Canvas>().transform);
            
            Image dragImage = dragPreview.gameObject.AddComponent<Image>();
            dragImage.sprite = iconImage.sprite;
            dragImage.raycastTarget = false;
            
            RectTransform dragPreviewRect = dragPreview.GetComponent<RectTransform>();
            dragPreviewRect.position = eventData.position;
            dragPreviewRect.sizeDelta = ((RectTransform) transform).sizeDelta;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (GetCurrentItem() == null)
                return;
            
            if (dragPreview == null)
                return;

            // Move drag preview
            dragPreview.transform.position = eventData.position;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (GetCurrentItem() == null)
                return;

            // Destroy drag preview
            // Calculate mouse over slot
            // Move item to slot
            Destroy(dragPreview);

            if (eventData.hovered.Count <= 0)
            {
                // Drop item reference
                ExecuteDrop();
                return;
            }

            foreach (var go in eventData.hovered)
            {
                var hoverSlot = go.GetComponentInParent<ItemSlotRendererBase>();
                if (hoverSlot != null)
                {
                    ExecuteMove(hoverSlot);
                    break;
                }
            }
        }

        protected abstract void ExecuteUse();

        protected abstract void ExecuteDrop();

        protected abstract void ExecuteMove(ItemSlotRendererBase hoverSlot);
        
        public abstract ItemReference GetCurrentItem();
    }
}