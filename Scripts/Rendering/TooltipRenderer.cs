﻿using NetworkAuthority.Inventory.UI.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace NetworkAuthority.Inventory.UI
{
    public class TooltipRenderer : UIBehaviour
    {
        [SerializeField] private TMP_Text itemNameText = null;
        [SerializeField] private TMP_Text itemDescriptionText = null;
        [SerializeField] private Image itemImage = null;
        [SerializeField] private float fadeInTime = 0.1f;

        private ItemReference currentItem;
        private bool draw;
        private bool isDrawing;
        private static TooltipRenderer _instance;
        private Graphic[] _graphics;
        private Canvas _canvas;
        private RectTransform _rectTransform;

        protected override void Awake()
        {
            base.Awake();

            _canvas = GetComponentInParent<Canvas>();
            _instance = this;
            _graphics = GetComponentsInChildren<Graphic>(true);
            _rectTransform = (RectTransform)transform;
        }

        protected override void Start()
        {
            if (EventSystem.current == null)
            {
                Debug.Log("Tooltip Renderer: Event system not found!");
                enabled = false;
            }
        }

        protected virtual void Update()
        {
            if (draw)
            {
                Move();
                if (!isDrawing)
                {
                    ForceUpdateLayout();
                    ResetAlpha();
                    isDrawing = true;
                }
            }
            else if (isDrawing)
            {
                SetAlpha(0, 0);
                currentItem = null;
                isDrawing = false;
            }

            draw = false;
        }

        private void ResetAlpha()
        {
            SetAlpha(0f, 0f);
            SetAlpha(1f, fadeInTime);
        }

        private void ForceUpdateLayout()
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(_rectTransform);
            _rectTransform.ForceUpdateRectTransforms();
        }

        protected virtual void Move()
        {
            if (EventSystem.current.currentInputModule == null)
            {
                Debug.Log("Tooltip Renderer: Current input module not found!");
                enabled = false;
                return;
            }

            var mousePos = EventSystem.current.currentInputModule.input.mousePosition;
            var position = UIUtils.KeepWithinScreenBounds(_canvas, _rectTransform, mousePos);
            transform.position = position;
        }

        private void InternalDrawTooltip(ItemReference reference)
        {
            if (currentItem != null && currentItem != reference)
                return;

            currentItem = reference;
            itemImage.sprite = currentItem.asset.icon;
            itemNameText.text = currentItem.asset.itemName;
            itemDescriptionText.text = currentItem.asset.GetDescription();
            draw = true;
        }

        private void SetAlpha(float alpha, float time)
        {
            foreach (Graphic g in _graphics)
                g.CrossFadeAlpha(alpha, time, true);
        }

        /// <summary>
        /// Draw a tooltip for the given item reference.
        /// </summary>
        /// <param name="reference">The item reference.</param>
        public static void DrawTooltip(ItemReference reference)
        {
            if (_instance == null) return;
            _instance.InternalDrawTooltip(reference);
        }
    }
}