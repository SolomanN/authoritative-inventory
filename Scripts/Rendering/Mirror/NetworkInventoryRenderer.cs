#if MIRROR
using Mirror;
#endif
using UnityEngine;

namespace NetworkAuthority.Inventory.UI
{
    #if MIRROR
    [RequireComponent(typeof(NetworkIdentity))]
    public class NetworkInventoryRenderer : InventoryRenderer, INetworkInitCallbacks
    {
        private bool isServer = false;

        public override void Initialize()
        {
            // Override the default awake spawn.
            Debug.Log("Inventory: Overriding default renderer initialize and awaiting network authority.");
        }
        
        public void OnStartAuthority()
        {
            if (isServer) return;
            // Actually initialize the inventory renderers.
            base.Initialize();
        }

        public void OnStartServer()
        {
            isServer = true;
        }

        public void OnStartClient()
        {
            isServer = false;
        }
        
        public void OnStartLocalPlayer() { }
        public void OnStopAuthority() { }
    }
    #endif
}