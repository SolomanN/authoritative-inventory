#if MIRROR
using System;
using System.Linq;
using Mirror;
using UnityEngine;
// ReSharper disable UnusedParameter.Local

namespace NetworkAuthority.Inventory
{
    [RequireComponent(typeof(Inventory))]
    [DisallowMultipleComponent]
    public class MirrorInventoryManager : NetworkBehaviour, IInventoryAuthorizable, IInventoryCallbacks
    {
        private Inventory inventory;
        private bool suppress;
        
        /// <summary>
        /// The authoritative client.
        /// </summary>
        private NetworkConnection client => netIdentity.clientAuthorityOwner;

        public void Initialize(Inventory inventory)
        {
            this.inventory = inventory;
        }

        public bool AuthorizeItemAdd(ItemInstanceBase item)
        {
            if (isServer) return true;
            CmdAddItem(item.gameObject);
            return false;
        }

        public bool AuthorizeItemDrop(ItemCollection collection, ItemReference item)
        {
            if (isServer) return true;
            CmdDropItem(new SlotCommand(item, collection));
            collection[collection.GetSlot(item)] = null;
            item.OnDropped();
            return false;
        }

        public bool AuthorizeItemDestroy(ItemInstanceBase item)
        {
            if (isServer) NetworkServer.Destroy(item.gameObject);
            return false;
        }

        public bool AuthorizeItemUse(ItemCollection collection, ItemReference itemReference)
        {
            if (isServer) return true;
            CmdUseItem(new SlotCommand(itemReference, collection));
            return false;
        }

        public bool AuthorizeItemMove(ItemReference reference, ItemCollection collection, int slot)
        {
            if (isServer) return true;
            // If the collection already contains this item reference we will trust the client
            // to move it.
            bool trust = collection.Contains(reference);
            CmdMoveItem(new MoveItemCommand(reference, collection, slot, trust));
            return trust;
        }

        public void OnStackChanged(ItemCollection collection, ItemReference reference)
        {
            if (suppress) return;
            if (!isServer) return;
            TargetRpcItemStackChanged(client, new StackChangedRpc(collection, reference));
        }

        public void OnSlotChanged(ItemCollection collection, ItemReference reference, int slot)
        {
            if (suppress) return;
            if (!isServer) return;
            TargetRpcSlotChanged(client, new SlotChangedRpc(collection, reference));
        }

        public void OnSlotCleared(ItemCollection collection, int slot)
        {
            if (suppress) return;
            if (!isServer) return;
            TargetRpcSlotCleared(client, new SlotClearedRpc(collection, slot));
        }

        public void OnItemInstanceCreated(ItemInstanceBase instance)
        {
            if (!isServer) return;
            NetworkServer.Spawn(instance.gameObject);
        }

        #region Client

        [TargetRpc]
        private void TargetRpcItemStackChanged(NetworkConnection conn, StackChangedRpc stackChanged)
        {
            stackChanged.Output(inventory, out var reference, out var stack);
            reference.itemStack = stack;
        }

        [TargetRpc]
        private void TargetRpcSlotChanged(NetworkConnection conn, SlotChangedRpc slotChanged)
        {
            slotChanged.Output(inventory, out var collection, out var reference, out var slot);
            collection[slot] = reference;
        }

        [TargetRpc]
        private void TargetRpcSlotCleared(NetworkConnection conn, SlotClearedRpc slotCleared)
        {
            slotCleared.Output(inventory, out var collection, out var slot);
            collection[slot] = null;
        }

        #endregion

        #region Server

        [Command]
        private void CmdAddItem(GameObject itemObject)
        {
            if (itemObject == null) return;
            ItemInstanceBase itemInstance = itemObject.GetComponent<ItemInstanceBase>();
            if (itemInstance.itemStack == 0) return;
            inventory.Add(itemInstance);
        }

        [Command]
        private void CmdUseItem(SlotCommand useCommand)
        {
            useCommand.Output(inventory, out var slot, out var collection);
            try
            {
                inventory.Use(collection[slot]);
            }
            catch (Exception e)
            {
                Debug.LogError($"Client {client.address} USE item failed. Possible cheating. Err: {e}");
            }
        }

        [Command]
        private void CmdMoveItem(MoveItemCommand moveCommand)
        {
            moveCommand.Output(inventory, out var fromCollection, out var toCollection, out var fromSlot, out var toSlot);
            
            // Suppress the move callbacks if we're trusting the client.
            suppress = moveCommand.trusted;
            try
            {
                inventory.Move(fromCollection[fromSlot], toCollection, toSlot);
            }
            catch(Exception e)
            {
                Debug.LogError($"Client {client.address} MOVE item failed. Possible cheating. Err: {e}");
            }
            suppress = false;
        }

        [Command]
        private void CmdDropItem(SlotCommand dropCommand)
        {
            dropCommand.Output(inventory, out var slot, out var collection);
            try
            {
                inventory.Drop(collection[slot]);
            }
            catch (Exception e)
            {
                Debug.LogError($"Client {client.address} DROP item failed. Possible cheating. Err: {e}");
            }
        }

        #endregion

        #region Rpc Structs

        [Serializable]
        public struct SlotClearedRpc
        {
            public int collection, itemSlot;

            public SlotClearedRpc(ItemCollection collection, int slot)
            {
                this.collection = collection.collectionIndex;
                itemSlot = slot;
            }

            public void Output(Inventory inventory, out ItemCollection collection, out int slot)
            {
                collection = inventory.GetCollection(this.collection);
                slot = itemSlot;
            }
        }

        [Serializable]
        public struct StackChangedRpc
        {
            public int collection, itemSlot, stack;

            public StackChangedRpc(ItemCollection collection, ItemReference reference)
            {
                this.collection = collection.collectionIndex;
                itemSlot = collection.GetSlot(reference);
                stack = reference.itemStack;
            }

            public void Output(Inventory inventory, out ItemReference reference, out int stack)
            {
                var collection = inventory.GetCollection(this.collection);
                reference = collection[itemSlot];
                stack = this.stack;
            }
        }

        [Serializable]
        public struct SlotChangedRpc
        {
            public int collection, slot, stack, maxStack;
            public Guid assetId, referenceId;
            
            public SlotChangedRpc(ItemCollection collection, ItemReference reference)
            {
                this.collection = collection.collectionIndex;
                slot = collection.GetSlot(reference);
                stack = reference.itemStack;
                maxStack = reference.maxItemStack;
                assetId = Guid.Parse(reference.asset.itemId);
                referenceId = reference.id;
            }

            public void Output(Inventory inventory, out ItemCollection collection, out ItemReference reference, out int slot)
            {
                LoadItem(inventory, out reference);
                collection = inventory.GetCollection(this.collection);
                slot = this.slot;
            }

            private void LoadItem(Inventory inventory, out ItemReference reference)
            {
                Guid itemId = referenceId;
                var oldCollection = inventory.itemCollections.FirstOrDefault(x => x.Contains(itemId));
                var existingItem = oldCollection?.FirstOrDefault(x => x != null && x.id == itemId);
                if (existingItem != null)
                {
                    reference = existingItem;
                }
                else
                {
                    var asset = ItemDatabase.GetAsset(assetId) as ItemBase;
                    reference = new ItemReference(asset, stack, maxStack, itemId);
                }
            }
        }

        #endregion

        #region Command Structs

        [Serializable]
        public struct MoveItemCommand
        {
            public int fromCollection, fromSlot, toCollection, toSlot;
            public bool trusted;

            public MoveItemCommand(ItemReference item, ItemCollection collection, int slot, bool trusted = false)
            {
                Inventory inventory = collection.inventory;
                toCollection = inventory.GetCollectionIndex(collection);
                toSlot = slot;
                var itemCollection = inventory.itemCollections.First(x => x.Contains(item));
                fromCollection = itemCollection.collectionIndex;
                fromSlot = itemCollection.GetSlot(item);
                this.trusted = trusted;
            }

            public void Output(Inventory inventory, out ItemCollection fromCollection, out ItemCollection toCollection, out int fromSlot, out int toSlot)
            {
                fromCollection = inventory.GetCollection(this.fromCollection);
                toCollection = inventory.GetCollection(this.toCollection);
                fromSlot = this.fromSlot;
                toSlot = this.toSlot;
            }
        }

        [Serializable]
        public struct SlotCommand
        {
            public int slot, collection;

            public SlotCommand(ItemReference reference, ItemCollection collection)
            {
                slot = collection.GetSlot(reference);
                this.collection = collection.collectionIndex;
            }

            public void Output(Inventory inventory, out int slot, out ItemCollection collection)
            {
                slot = this.slot;
                collection = inventory.GetCollection(this.collection);
            }
        }

        #endregion
    }
}
#endif