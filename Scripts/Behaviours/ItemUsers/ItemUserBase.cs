﻿using UnityEngine;

namespace NetworkAuthority.Inventory
{
    [RequireComponent(typeof(Inventory))]
    public abstract class ItemUserBase<TItemType> : MonoBehaviour, IItemUser where TItemType : ItemBase
    {
        public Inventory inventory { get; private set; }
        
        protected virtual void Start()
        {
            inventory = GetComponent<Inventory>();
            inventory.RegisterItemUser(this, typeof(TItemType));
        }

        public abstract void Use(ItemCollection collection, ItemReference reference);
    }
}
