using NetworkAuthority.Inventory.PropertyDrawers;
using UnityEngine;

namespace NetworkAuthority.Inventory
{
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    public abstract partial class ItemInstanceBase : MonoBehaviour, IStackable
    {
        [SerializeField, Disabled] private ItemBase itemAsset = null;
        [SerializeField] private int stack = 0;
        [SerializeField] private int maxStack = 1;

        /// <summary>
        /// The item stack.
        /// </summary>
        public int itemStack => stack;
        
        /// <summary>
        /// The maximum item stack.
        /// </summary>
        public int maxItemStack => maxStack;
        
        /// <summary>
        /// The item asset.
        /// </summary>
        public ItemBase asset => itemAsset;

        /// <summary>
        /// Sets the item stack.
        /// </summary>
        /// <param name="value">The new stack.</param>
        public void SetStack(int value)
        {
            stack = value;
        }
    }
}