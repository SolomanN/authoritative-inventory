using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;

namespace NetworkAuthority.Inventory
{
    [DisallowMultipleComponent]
    public class Inventory : MonoBehaviour
    {
        [SerializeField] private ItemCollection[] collections = null;

        [CanBeNull] private IInventoryAuthorizable _authorizer;
        private IInventoryCallbacks[] _callbacks;
        
        private Dictionary<string, ItemCollection> collectionNames;
        private List<Tuple<Type, IItemUser>> users;

        /// <summary>
        /// The item collections this inventory contains.
        /// </summary>
        public IEnumerable<ItemCollection> itemCollections => collections;
        
        private void Awake()
        {
            collectionNames = new Dictionary<string, ItemCollection>();
            users = new List<Tuple<Type, IItemUser>>();
            
            for (var i = 0; i < collections.Length; i++)
            {
                var collection = collections[i];
                collection.Initialize(this, i);
                collectionNames[collection.collectionName] = collection;
            }

            _authorizer = GetComponent<IInventoryAuthorizable>();
            _callbacks = GetComponents<IInventoryCallbacks>();
            _authorizer?.Initialize(this);
        }

        /// <summary>
        /// Add an item instance.
        /// </summary>
        /// <param name="instance">The item instance.</param>
        public void Add(ItemInstanceBase instance)
        {
            if (!_authorizer?.AuthorizeItemAdd(instance) ?? false) return;
            
            // Get the first collection.
            var collection = collections.First(x => x.AllowsCategory(instance.asset.category));
            if (collection == null)
                return;
    
            // Create an item reference from the instance.
            var reference = ItemReference.FromInstance(instance);
                
            // Find any existing references.
            var existingReferences = collection.FindAll(x => x != null && !x.IsFull() && x.asset.itemId == instance.asset.itemId);
            if (existingReferences.Length > 0)
            {
                // Loop through all existing references to this item.
                foreach (var existing in existingReferences)
                {
                    Combine(reference, existing);
    
                    // If the reference is empty, there's nothing left to do.
                    if (reference.IsEmpty())
                        break;
                }
            }
                
            // The reference was completely used.
            if (reference.IsEmpty())
            {
                // Destroy the object.
                Destroy(instance);
                    
                // The reference is empty so return.
                return;
            }
                
            // Get the first empty slot.
            int firstSlot = collection.GetEmptySlot(instance.asset.category);
            if (firstSlot == -1)
            {
                // There's no empty slots in the collection.
                return;
            }
                
            // Otherwise if the reference is not empty, then we need to 
            // add the item to the collection in an empty slot.
            collection[firstSlot] = reference;
                
            // Make sure to notify the controller that the slot has updated.
            OnSlotChanged(collection, reference, firstSlot);
                
            // Destroy the object.
            Destroy(instance);
        }

        /// <summary>
        /// Move an item reference to the given slot.
        /// </summary>
        /// <param name="reference">The item reference.</param>
        /// <param name="collection">The item collection to move to.</param>
        /// <param name="slot">The slot in the collection.</param>
        public void Move(ItemReference reference, ItemCollection collection, int slot)
        {
            if (!_authorizer?.AuthorizeItemMove(reference, collection, slot) ?? false) return;
            if (!collection.DoesSlotAllow(slot, reference.asset.category)) return;

            ItemCollection currentCollection = collections.First(x => x.Contains(reference));
            int currentSlot = currentCollection.GetSlot(reference);
            
            if (collection[slot] != null)
            {
                ItemReference existingReference = collection[slot];
                if (currentCollection.DoesSlotAllow(currentSlot, existingReference.asset.category))
                {
                    if (existingReference.IsTheSameAs(reference) && !existingReference.IsFull())
                    {
                        Combine(reference, existingReference);
                    }
                    else
                    {
                        Swap(reference, existingReference);
                    }
                }
            }
            else
            {
                // Swap slots
                currentCollection[currentSlot] = null;
                collection[slot] = reference;
            
                OnSlotCleared(currentCollection, currentSlot);
                OnSlotChanged(collection, reference, slot);
            }
        }

        /// <summary>
        /// Destroy the given item instance.
        /// </summary>
        /// <param name="item">The item instance.</param>
        public void Destroy(ItemInstanceBase item)
        {
            if (!_authorizer?.AuthorizeItemDestroy(item) ?? false) return;
            Destroy(item.gameObject);
        }

        /// <summary>
        /// Drop an item instance.
        /// </summary>
        /// <param name="itemReference">The item instance.</param>
        public void Drop(ItemReference itemReference)
        {
            ItemCollection collection = collections.First(x => x.Contains(itemReference));
            if (collection == null) return;
            if (!_authorizer?.AuthorizeItemDrop(collection, itemReference) ?? false) return;
            
            int slot = collection.GetSlot(itemReference);
            collection[slot] = null;
            OnSlotCleared(collection, slot);

            GameObject prefab = Instantiate(itemReference.asset.instancePrefab);
            ItemInstanceBase instance = prefab.GetComponent<ItemInstanceBase>();
            instance.SetStack(itemReference.itemStack);
            itemReference.OnDropped();

            OnItemInstanceCreated(instance);
        }

        /// <summary>
        /// Use the item in the given collection.
        /// </summary>
        /// <param name="itemReference">The item reference.</param>
        public void Use(ItemReference itemReference)
        {
            ItemCollection collection = collections.First(x => x.Contains(itemReference));
            if (collection == null) return;
            if (!_authorizer?.AuthorizeItemUse(collection, itemReference) ?? false) return;
            foreach (var (type, user) in users)
            {
                if (type.IsInstanceOfType(itemReference.asset))
                    user.Use(collection, itemReference);
            }
        }

        /// <summary>
        /// Register an item user.
        /// </summary>
        /// <param name="user">The item user.</param>
        /// <param name="itemType">The item type.</param>
        public void RegisterItemUser(IItemUser user, Type itemType)
        {
            var item = new Tuple<Type, IItemUser>(itemType, user);
            users.Add(item);
        }

        /// <summary>
        /// Get the first collection with the given name.
        /// </summary>
        /// <param name="collectionName"></param>
        /// <returns></returns>
        public ItemCollection GetCollection(string collectionName)
        {
            return collectionNames.TryGetValue(collectionName, out var col) ? col : null;
        }

        /// <summary>
        /// Get the first collection with the given index.
        /// </summary>
        /// <param name="collectionIndex">The collection index.</param>
        /// <returns></returns>
        public ItemCollection GetCollection(int collectionIndex)
        {
            try
            {
                return collections[collectionIndex];
            }
            catch
            {
                return null;
            }
        }
        
        /// <summary>
        /// Gets the collection index.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        public int GetCollectionIndex(ItemCollection collection)
        {
            return Array.IndexOf(collections, collection);
        }
        
        private void Swap(ItemReference itemA, ItemReference itemB)
        {
            ItemCollection itemACollection = collections.First(x => x.Contains(itemA));
            ItemCollection itemBCollection = collections.First(x => x.Contains(itemB));

            int itemASlot = itemACollection.GetSlot(itemA);
            int itemBSlot = itemBCollection.GetSlot(itemB);

            itemBCollection[itemBSlot] = itemA;
            itemACollection[itemASlot] = itemB;
            
            OnSlotChanged(itemBCollection, itemA, itemBSlot);
            OnSlotChanged(itemACollection, itemB, itemASlot);
        }

        private void Combine(ItemReference source, ItemReference target)
        {
            ItemCollection sourceCollection = collections.First(x => x.Contains(source));
            ItemCollection targetCollection = collections.First(x => x.Contains(target));
            
            var needed = target.maxItemStack - target.itemStack;
            for (int j = 0; j < needed; j++)
            {
                if (target.IsFull()) break;
                if (source.IsEmpty()) break;
                target.itemStack++;
                source.itemStack--;
            }

            if (targetCollection != null)
            {
                OnStackChanged(targetCollection, target);
            }

            if (sourceCollection != null && source.IsEmpty())
            {
                int sourceSlot = sourceCollection.GetSlot(source);
                sourceCollection[sourceSlot] = null;
                OnSlotCleared(sourceCollection, sourceSlot);
            }
        }

        private void OnSlotChanged(ItemCollection collection, ItemReference reference, int slot)
        {
            foreach (var callback in _callbacks)
                callback.OnSlotChanged(collection, reference, slot);
        }

        private void OnStackChanged(ItemCollection collection, ItemReference reference)
        {
            foreach (var callback in _callbacks)
                callback.OnStackChanged(collection, reference);
        }

        private void OnSlotCleared(ItemCollection collection, int slot)
        {
            
            foreach (var callback in _callbacks)
                callback.OnSlotCleared(collection, slot);
        }
        
        private void OnItemInstanceCreated(ItemInstanceBase instance)
        {
            foreach (var callback in _callbacks)
                callback.OnItemInstanceCreated(instance);
        }
    }
}