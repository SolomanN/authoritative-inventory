#if UNITY_EDITOR
namespace NetworkAuthority.Inventory
{
    public abstract partial class ItemInstanceBase
    {
        /// <summary>
        /// (Editor Only) Sets the item asset.
        /// </summary>
        /// <param name="val"></param>
        public void SetAsset(ItemBase val)
        {
            itemAsset = val;
        }
    }
}
#endif