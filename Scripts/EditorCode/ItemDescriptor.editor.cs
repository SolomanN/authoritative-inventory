#if UNITY_EDITOR
using System;
using UnityEngine;

namespace NetworkAuthority.Inventory
{
    public abstract partial class ItemDescriptor
    {
        [SerializeField, HideInInspector]
        private int instanceID;

        /// <summary>
        /// (Editor Only) Will be called when the asset is imported or re-imported.
        /// </summary>
        /// <returns></returns>
        public bool PostInitialize()
        {
            // New item check.
            if (instanceID != GetInstanceID())
            {
                // Call first time initialize on the asset. 
                Construct();
                instanceID = GetInstanceID();
                return true;
            }

            return false;
        }
        
        protected virtual void Construct()
        {
            _name = name;
            GenerateId();
        }

        private void GenerateId()
        {
            _id = Guid.NewGuid().ToString();
            Debug.Log($"Inventory: Assigned ID '{_id}' to '{name}'"); 
        }
    }
}
#endif