#if UNITY_EDITOR
using UnityEngine;

namespace NetworkAuthority.Inventory
{
    public abstract partial class ItemBase
    {
        [HideInInspector, SerializeField]
        private GameObject _oldInstancePrefab = null;

        protected override void Construct()
        {
            base.Construct();
            instancePrefab = null;
            _oldInstancePrefab = null;
        }

        /// <summary>
        /// A custom OnValidate function used by the ItemBaseEditor.
        /// </summary>
        public void EditorChanged()
        {
            InstanceChangeCheck();
        }

        private void InstanceChangeCheck()
        {
            // If the assigned instance object is not null.
            if (instancePrefab != null)
            {
                // The instance object has changed...
                if (instancePrefab != _oldInstancePrefab)
                {
                    // Get the instance base component.
                    ItemInstanceBase instanceBase = instancePrefab.GetComponent<ItemInstanceBase>();
                    if (instanceBase != null)
                    {
                        // The instance already has an asset defined.
                        if (instanceBase.asset != null && instanceBase.asset != this)
                        {
                            // Ask to change the asset.
                            if (UnityEditor.EditorUtility.DisplayDialog(
                                "Item Asset Reference Change",
                                $"The given item instance {instanceBase.name} already has an asset reference. Are you sure you want to replace the instance's reference with {name}?",
                                "Yeah", "Nevermind"))
                            {
                                
                                // Make sure to remove the reference from the current asset.
                                instanceBase.asset.instancePrefab = null;
                                instanceBase.asset._oldInstancePrefab = null;
                            }
                            // If we don't want to change the asset then just return.
                            else
                            {
                                instancePrefab = null;
                                _oldInstancePrefab = null;
                                return;
                            }
                        }
                                
                        // Make sure the old object asset is reset.
                        if (_oldInstancePrefab != null)
                        {
                            _oldInstancePrefab.GetComponent<ItemInstanceBase>().SetAsset(null);
                            UnityEditor.PrefabUtility.SavePrefabAsset(_oldInstancePrefab);
                        }

                        // Set the asset to this!
                        instanceBase.SetAsset(this);
                        UnityEditor.PrefabUtility.SavePrefabAsset(instanceBase.gameObject);
                        
                        Debug.Log($"Inventory: Set {instancePrefab.name}'s item reference to {name}");
                    }
                    else
                    {
                        // If the instance base component is null then we want to keep our reference null.
                        instancePrefab = null;
                        Debug.Log($"Inventory: Instance object must have an {nameof(ItemBase)} component.");
                    }
                }
            }

            // If the instance object ends up being null be our last one isn't.
            if (instancePrefab == null && _oldInstancePrefab != null)
            {
                // Then make sure to set the last instance's to null.
                ItemInstanceBase inst = _oldInstancePrefab.GetComponent<ItemInstanceBase>();
                inst.SetAsset(null);
            }

            _oldInstancePrefab = instancePrefab;
        }
    }
}
#endif