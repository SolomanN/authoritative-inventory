#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace NetworkAuthority.Inventory
{
    public partial class ItemDatabase
    {
        public bool syncWithAssets = true;
        [HideInInspector]
        public string syncRoot = string.Empty;

        public void Reload(bool log = true)
        {
            if (PrefabUtility.IsPartOfPrefabInstance(gameObject))
            {
                GameObject prefab = PrefabUtility.GetCorrespondingObjectFromSource(gameObject);
                ItemDatabase db = prefab.GetComponent<ItemDatabase>();
                db.OnReload(log);
                PrefabUtility.SavePrefabAsset(prefab);
                return;
            }
            
            OnReload(log);
            EditorUtility.SetDirty(gameObject);
        }

        private void OnReload(bool log)
        {
            itemAssets = new List<ItemDescriptor>();

            string[] GUIDs = AssetDatabase.FindAssets($"t:{nameof(ItemDescriptor)}");
            foreach (string guid in GUIDs)
            {
                string path = AssetDatabase.GUIDToAssetPath(guid);
                ItemDescriptor desc = AssetDatabase.LoadAssetAtPath<ItemDescriptor>(path);
                itemAssets.Add(desc);
            }

            if (log)
            {
                Debug.Log($"Item Database: '{gameObject.name}' reloaded with {itemAssets.Count} assets.");
            }
        }
    }
}
#endif