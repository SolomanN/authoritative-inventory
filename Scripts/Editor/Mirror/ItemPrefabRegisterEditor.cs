﻿#if MIRROR
using System;
using System.Collections.Generic;
using System.Linq;
using Mirror;
using NetworkAuthority.Inventory;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

internal static class ItemPrefabRegisterEditor 
{
    [InitializeOnLoadMethod]
    private static void Init()
    {
        EditorSceneManager.sceneOpened += OnSceneOpened;
        EditorSceneManager.sceneSaved += OnSceneSaved;
    }

    private static void OnSceneSaved(Scene scene)
    {
        CheckScene(scene);
    }

    private static void OnSceneOpened(Scene scene, OpenSceneMode mode)
    {
        CheckScene(scene);
    }

    private static void CheckScene(Scene scene)
    {
        if (!scene.IsValid())
            return;
    
        NetworkManager manager = FindNetworkManager(scene);
        if (manager == null) return;

        string[] guids = AssetDatabase.FindAssets($"t:{nameof(ItemBase)}");
        List<GameObject> prefabs = new List<GameObject>();
        foreach (string guid in guids)
        {
            string path = AssetDatabase.GUIDToAssetPath(guid);
            ItemBase itemAsset = AssetDatabase.LoadAssetAtPath<ItemBase>(path);
            if (itemAsset.instancePrefab != null)
            {
                prefabs.Add(itemAsset.instancePrefab);
            }
        }
        
        List<GameObject> missingPrefabs = new List<GameObject>();
        foreach (var prefab in prefabs)
        {
            if (manager.spawnPrefabs.Contains(prefab))
                continue;
            missingPrefabs.Add(prefab);
        }
        
        if (missingPrefabs.Count <= 0)
            return;
        
        if (EditorUtility.DisplayDialog("Items not registered",
            $"There are {missingPrefabs.Count} missing Inventory Item Prefabs in the Network Manager. Would you like to add them now?",
            "Yes", "No"))
        {
            manager.spawnPrefabs.AddRange(missingPrefabs);
            EditorSceneManager.SaveScene(scene);
        }
    }

    private static NetworkManager FindNetworkManager(Scene scene)
    {
        GameObject[] rootGameObjects = scene.GetRootGameObjects();
        if (rootGameObjects.Length <= 0)
            return null;
        return Array
            .FindAll(rootGameObjects, x => x.GetComponentInChildren<NetworkManager>()).FirstOrDefault()?
            .GetComponent<NetworkManager>();
    }
}
#endif