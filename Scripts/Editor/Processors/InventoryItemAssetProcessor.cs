﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace NetworkAuthority.Inventory.Editors
{
    public class InventoryItemAssetProcessor : AssetPostprocessor
    {
        private static List<string> paths;
        
        private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets,
            string[] movedFromAssetPaths)
        {
            paths = new List<string>();
            if (importedAssets.Length > 0)
            {
                foreach (string assetPath in importedAssets)
                {
                    if (movedAssets.Any(x => x == assetPath))
                        return;
                    
                    Type t = AssetDatabase.GetMainAssetTypeAtPath(assetPath);
                    if (typeof(ItemDescriptor).IsAssignableFrom(t))
                    {
                        ItemDescriptor item = AssetDatabase.LoadAssetAtPath<ItemDescriptor>(assetPath);
                        if (item.PostInitialize())
                        {
                            paths.Add(assetPath);
                        }
                    }
                }
            }

            if (paths.Count > 0)
            {
                RegenerateItemDatabases();
            }
        }

        private static void RegenerateItemDatabases()
        {
            string[] GUIDs = AssetDatabase.FindAssets("t:Prefab");
            foreach (string guid in GUIDs)
            {
                string path = AssetDatabase.GUIDToAssetPath(guid);
                GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(path);
                ItemDatabase database = prefab.GetComponent<ItemDatabase>();
                if (database != null)
                {
                    if (database.syncWithAssets && paths.Any(x => x.Contains(database.syncRoot)))
                        database.Reload();
                }
            }
        }
    }
}
