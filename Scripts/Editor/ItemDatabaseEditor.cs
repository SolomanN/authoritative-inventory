using UnityEditor;
using UnityEngine;

namespace NetworkAuthority.Inventory.Editors
{
    [CustomEditor(typeof(ItemDatabase))]
    public class ItemDatabaseEditor : Editor
    {
        private ItemDatabase itemDatabase;
        private SerializedProperty syncPathProperty;

        private void OnEnable()
        {
            itemDatabase = (ItemDatabase) target;
            syncPathProperty = serializedObject.FindProperty(nameof(ItemDatabase.syncRoot));
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            EditorGUILayout.BeginHorizontal();

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                EditorGUILayout.PropertyField(syncPathProperty);
                if (GUILayout.Button("...", EditorStyles.miniButton, GUILayout.Width(25)))
                {
                    string path = EditorUtility.OpenFolderPanel("Select Item Database Sync Path", null, null);
                    syncPathProperty.stringValue = path.Replace(Application.dataPath + "/", string.Empty);
                }
                EditorGUILayout.EndHorizontal();

                if (check.changed)
                {
                    serializedObject.ApplyModifiedProperties();
                }
            }
            
            if (GUILayout.Button("Reload"))
                itemDatabase.Reload();
        }
    }
}