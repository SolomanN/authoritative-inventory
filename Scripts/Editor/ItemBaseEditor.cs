using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace NetworkAuthority.Inventory.Editors
{
    [CustomEditor(typeof(ItemBase), true)]
    public class ItemBaseEditor : Editor
    {
        private ItemBase itemBase;
        private SerializedProperty instancePrefabProp;
        private Editor previewEditor;
        private int objectPickerID;
        
        private void OnEnable()
        {
            itemBase = (ItemBase) target;
            instancePrefabProp = serializedObject.FindProperty(nameof(ItemBase.instancePrefab));
        }

        public override void OnInspectorGUI()
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                base.OnInspectorGUI();

                if (Event.current.type == EventType.ExecuteCommand && Event.current.commandName == "ObjectSelectorUpdated")
                {
                    GameObject obj = (GameObject)EditorGUIUtility.GetObjectPickerObject();
                    instancePrefabProp.objectReferenceValue = obj;
                    Apply();
                    return;
                }
                
                if (instancePrefabProp.objectReferenceValue == null)
                {
                    if (GUILayout.Button("Create Prefab"))
                    {
                        var menu = new GenericMenu();
                        var types = FindTypes();
                        
                        foreach (var type in types)
                        {
                            menu.AddItem(new GUIContent(type.Name), false, CreatePrefab, type);
                        }
                        
                        menu.ShowAsContext();
                    }

                    if (GUILayout.Button("Select Existing Prefab"))
                    {
                        PickObject();
                    }
                }
                else
                {
                    if (GUILayout.Button("Open Prefab"))
                    {
                        AssetDatabase.OpenAsset(instancePrefabProp.objectReferenceValue);
                    }
                    
                    if (GUILayout.Button("Replace Prefab"))
                    {
                        PickObject();
                    }
                }
                
                if (check.changed)
                {
                    Apply();
                }
            }
        }

        private void CreatePrefab(object userData)
        {
            var type = (Type) userData;
            
            var openFolderPath = EditorUtility.OpenFolderPanel("Select prefab output path", null, null);
            if (string.IsNullOrEmpty(openFolderPath))
            {
                Debug.Log("Inventory: Cancelled prefab create");
                return;
            }
            
            openFolderPath = openFolderPath.Replace(Application.dataPath, string.Empty).Replace("\\", "/");
            string path = Path.Combine("Assets", openFolderPath.Length > 0 ? openFolderPath.Remove(0, 1) : openFolderPath);

            if (!AssetDatabase.IsValidFolder(path))
            {
                Debug.LogWarning($"Inventory: Path {path} invalid.");
                return;
            }
            
            var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.AddComponent(type);
            var prefab = PrefabUtility.SaveAsPrefabAsset(cube, $"{path}/{type.Name}.prefab");
            DestroyImmediate(cube);

            instancePrefabProp.objectReferenceValue = prefab;
            Apply();
        }

        private void PickObject()
        {
            EditorGUIUtility.ShowObjectPicker<GameObject>(instancePrefabProp.objectReferenceValue, false, string.Empty,
                objectPickerID = GUIUtility.GetControlID(FocusType.Passive));
        }

        private void Apply()
        {
            serializedObject.ApplyModifiedProperties();
            itemBase.EditorChanged();
        }

        public override bool HasPreviewGUI()
        {
            return true;
        }

        public override void OnPreviewGUI(Rect r, GUIStyle background)
        {
            if (instancePrefabProp.objectReferenceValue != null)
            {
                GameObject obj = (GameObject)instancePrefabProp.objectReferenceValue;
                CreateCachedEditor(obj, null, ref previewEditor);
                previewEditor.OnPreviewGUI(r, background);
                EditorGUILayout.LabelField($"Prefab: {obj.name}", EditorStyles.centeredGreyMiniLabel);
            }
            else
            {
                EditorGUILayout.LabelField("No prefab instance", EditorStyles.centeredGreyMiniLabel);
                base.OnPreviewGUI(r, background);
            }
            
            EditorGUILayout.LabelField($"Item ID: {itemBase.itemId}", EditorStyles.centeredGreyMiniLabel);
        }
        
        private IEnumerable<Type> FindTypes()
        {
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            List<Type> foundTypes = new List<Type>();
            foreach (Assembly ass in assemblies)
            {
                Type[] types = ass.GetTypes();
                foreach (Type type in types)
                {
                    if (type.IsAbstract) continue;
                    if (!typeof(ItemInstanceBase).IsAssignableFrom(type)) continue;
                    foundTypes.Add(type);
                }
            }
            return foundTypes.ToArray();
        }
    }
}