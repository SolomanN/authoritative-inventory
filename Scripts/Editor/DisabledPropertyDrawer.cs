using NetworkAuthority.Inventory.PropertyDrawers;
using UnityEditor;
using UnityEngine;

namespace NetworkAuthority.Inventory.Editors
{
    [CustomPropertyDrawer(typeof(DisabledAttribute))]
    public class DisabledPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            GUI.enabled = false;
            EditorGUI.PropertyField(position, property, label);
            GUI.enabled = true;
        }
    }
}