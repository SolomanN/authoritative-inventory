﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace NetworkAuthority.Inventory
{
    public abstract partial class ItemBase : ItemDescriptor
    {
        [SerializeField] private ItemCategory _category = null;
        [SerializeField] private Sprite _icon = null;
        [FormerlySerializedAs("_instancePrefab")] 
        [SerializeField, HideInInspector] public GameObject instancePrefab = null; 

        /// <summary>
        /// The category for this item.
        /// </summary>
        public ItemCategory category => _category;
        
        /// <summary>
        /// The item icon.
        /// </summary>
        public Sprite icon => _icon;
    }
}
