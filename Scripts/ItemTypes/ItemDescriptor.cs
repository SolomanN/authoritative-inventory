using UnityEngine;
using UnityEngine.Serialization;

namespace NetworkAuthority.Inventory
{
    public abstract partial class ItemDescriptor : ScriptableObject
    {
        [SerializeField] private string _name = null;
        [FormerlySerializedAs("description")] 
        [SerializeField, TextArea] private string _description = null;
        [FormerlySerializedAs("id")] 
        [SerializeField, HideInInspector]
        private string _id = null;

        /// <summary>
        /// The item name.
        /// </summary>
        public string itemName => _name;
        
        /// <summary>
        /// The item's description.
        /// </summary>
        public string itemDescription => _description;
        
        /// <summary>
        /// The item's ID.
        /// </summary>
        public string itemId => _id;

        public virtual string GetDescription()
        {
            return itemDescription;
        }
    }
}