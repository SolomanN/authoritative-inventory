using UnityEngine;

namespace NetworkAuthority.Inventory
{
    [CreateAssetMenu(menuName = "Inventory/Categories/New Item Category")]
    public class ItemCategory : ItemDescriptor
    { } 
}