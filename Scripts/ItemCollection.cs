using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace NetworkAuthority.Inventory
{
    [Serializable]
    public struct SlotMask
    {
        public int slotIndex;
        public ItemCategory[] categories;
    }
    
    [Serializable]
    public class ItemCollection : IReadOnlyCollection<ItemReference>
    {
        public delegate void SlotClearedDelegate(ItemCollection collection, int slot);
        public delegate void SlotChangeDelegate(ItemCollection collection, ItemReference item, int slot);
        
        /// <summary>
        /// Invoked when an item is added to a collection.
        /// </summary>
        public event SlotChangeDelegate SlotChanged;

        /// <summary>
        /// Invoked when an item slot is cleared.
        /// </summary>
        public event SlotClearedDelegate SlotCleared;

        [SerializeField] private string _collectionName = null;
        [SerializeField] private int collectionSize = 10;
        [SerializeField] private bool allowAllCategories = false;
        [SerializeField] private ItemCategory[] allowedCategories = null;
        [SerializeField] private SlotMask[] slotMasks = null;
        
        private ItemSlot[] slots;
        private ItemReference[] references;
        
        private Dictionary<int, SlotMask> slotMaskLookup;

        public int Count => slots.Length;
        public string collectionName => _collectionName;

        public ItemReference this[int slot]
        {
            get => slots[slot].currentItem;
            set
            {
                slots[slot].SetItem(value);
                references[slot] = value;

                if (value == null)
                {
                    SlotCleared?.Invoke(this, slot);
                }
                else
                {
                    SlotChanged?.Invoke(this, references[slot], slot);
                }
            }
        }
        public Inventory inventory { get; private set; }
        public int collectionIndex { get; private set; } = -1;

        public void Initialize(Inventory inventory, int index)
        {
            this.inventory = inventory;
            slots = new ItemSlot[collectionSize];
            references = new ItemReference[collectionSize];
            collectionIndex = index;
            
            for (int i = 0; i < slots.Length; i++)
            {
                slots[i] = new ItemSlot(this, i);
            }
        }

        public IEnumerator<ItemReference> GetEnumerator()
        {
            var enumerator = slots.GetEnumerator();
            while (enumerator.MoveNext())
            {
                yield return ((ItemSlot)enumerator.Current)?.currentItem;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public bool AllowsCategory(ItemCategory category)
        {
            if (allowAllCategories) return true;
            if (category == null) return false;
            return allowedCategories.Any(x => x.itemId == category.itemId);
        }

        public bool DoesSlotAllow(int slot, ItemCategory category)
        {
            InitSlotMaskLookup();
            if (slotMaskLookup.TryGetValue(slot, out var mask))
            {
                return mask.categories.Any(x => x == category);
            }

            return AllowsCategory(category);
        }

        public ItemReference[] FindAll(Predicate<ItemReference> func)
        {
            return Array.FindAll(references, func).ToArray();
        }

        public int GetSlot(ItemReference reference)
        {
            return Array.IndexOf(references, reference);
        }

        public ItemSlot GetSlot(int index)
        {
            return slots[index];
        }

        public int GetEmptySlot()
        {
            return Array.FindIndex(references, x => x == null);
        }

        public int GetEmptySlot(ItemCategory category)
        {
            return Array.FindIndex(slots, x => x.currentItem == null && DoesSlotAllow(x.index, category));
        }
        
        public void OnSlotChanged(ItemReference reference, int slot)
        {
            SlotChanged?.Invoke(this, reference, slot);
        }

        public void OnItemSlotCleared(int slot)
        {
            SlotCleared?.Invoke(this, slot);
        }

        private void InitSlotMaskLookup()
        {
            if (slotMaskLookup != null)
                return;
            
            slotMaskLookup = new Dictionary<int, SlotMask>();
            foreach (var mask in slotMasks)
            {
                slotMaskLookup[mask.slotIndex] = mask;
            }
        }

        public bool Contains(Guid referenceId)
        {
            return references.Any(x => x != null && x.id == referenceId);
        }
    }
}