using UnityEngine;

namespace NetworkAuthority.Inventory.PropertyDrawers
{
    public class DisabledAttribute : PropertyAttribute { }
}